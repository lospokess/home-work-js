/*
Створіть на сторінці div і дайте йому зовнішній відступ 150 пікселів. Використовуючи JS виведіть у консоль відступ
Створіть програму секундомір.
* Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"
* При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий * Виведення лічильників у форматі ЧЧ:ММ:СС
* Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції

*/
//!task1
/*
const div = document.body.prepend(document.createElement("div"));
console.log(getComputedStyle(div).margin);*/

//!task2
let btnCheck = false;
resetCheck = () => {//!Функція яка переписує значення
   btnCheck = false
}
const btnStart = document.getElementById("start"),//!шукає кнопку start по id
   btnStop = document.getElementById("stop"),//!шукає кнопку stop по id
   btnReset = document.getElementById("reset"),//!шукає кнопку reset по id
   min = document.getElementById("min"),//!-
   sec = document.getElementById("sec"),//!-  шукає місце відтворення цифр по id
   hor = document.getElementById("hour"),//!-
   stopWatch = document.querySelector(".container-stopwatch");
let seconds = '00',//!
   minute = '00',//! задаєм статичні значеня 
   hour = '00',//!
   sekundomir;//start 

btnStart.onclick = () => {
   if (!btnCheck) {//*
      btnCheck = true;//*перевірка якщо по кнопці натиснули більше натиснути не можна буде (тільки після скидання)
      clearCalss();
      stopWatch.classList.add("green");
      start();
   }

}

btnStop.onclick = () => {
   clearCalss();
   stopWatch.classList.add("red");
   stop();
   resetCheck();//!Функція яка переписує значення
}

btnReset.onclick = () => {
   clearCalss();                     //*ви далення кольорів
   stopWatch.classList.add("silver");//!доавання кольору
   reset();                          //*скидання лічильника
   resetCheck();                     //!Функція яка переписує значення
}
clearCalss = () => {//!Сттрілкова функ яка видаляє кольори
   stopWatch.classList.remove("red");
   stopWatch.classList.remove("black");
   stopWatch.classList.remove("green");
   stopWatch.classList.remove("silver");
}
/*
if(seckond < 10){
   sec.innerText = `0${seckond}`;
}else if(seckond >= 10 && seckond < 60){
   sec.innerText = `${seckond}`;
*/
showSec = () => {//! Функ як перевіряє значення виводу лічильника (корегує)
   if (seconds < 10) {
      sec.innerText = `0${seconds}`;
   } else if (seconds >= 10) {
      sec.innerText = `${seconds}`;
   }
   if (minute > 0 && minute < 10) {
      min.innerText = `0${minute}`;
   } else if (minute >= 10) {
      min.innerText = `${minute}`;
   }
   if (hour > 0 && hour < 10) {
      hor.innerText = `0${hour}`;
   } else if (hour >= 10) {
      hor.innerText = `${hour}`;
   }
}
//hor.innerText = hour;
//sec.innerText = seconds//`0${seckond}` ;
//min.innerText = minute;
start = () => { //! функ яка запускає лічильник
   sekundomir = setInterval(() => {
      //!(ДУМАЮ ЩО НЕ ПРАВИЛЬНИЙ СПОСІБ БО СЕКУНДОМІР ЛАГАЄ КОЛИ 59 ХВ ДОАЄТЬСЯ 1 ГОД АЛЕ ХВ НЕ СКИДУЄТЬСЯ НА 0 ЧЕКАЄ ПОКИ СЕКУНДИ ПРОЙДУТЬ 2 КОЛА ПОТІМ МІНЯЄ ЧИСЛО В ХВ НА ( 01 ) А КОЛИ ГОДИНА ДОХОДИТЬ ДО 23 ТАКОЖ НЕ СКИДАЄТЬСЯ НА 0 ЧЕКАЄ ТАКОЖ 2 КОЛА А ПОТІМ ПЕРЕХОДИТЬ НА (01) ВИХОДИТЬ ЗАМКНЕНЕ КОЛО  ЩО ( 00 : 00 : 00 ) НЕ ВИЙДЕ НІКОЛИ 
      if (seconds === 59) {
         seconds = '0';
         minute++;
      }
      if (minute === 59) {
         minute = '0';
         hour++
      }
      /*
      if (hour === 12) {
         hour = '00';
      }*/
      seconds++;
      showSec();
   }, 1000);//!(1000 цу інтервал виводу в даному випадку 1000 = 1c. )
}

stop = () => {//! функ яка зупиняє setInterval
   clearInterval(sekundomir);
}

reset = () => {//!функція  яка скидує значення секундоміру
   sec.innerText = seconds = "00";
   min.innerText = minute = "00";
   hor.innerText = hour = "00";
}
//! Як поліпшити мій код?
//! Можна пораду як навчитись грамотно кодити без хаосу в коді !?
//! Чи як багато хто каже В нього чистий код __що мені для цього потрібно?


/*
      seconds++;
      if (seconds > 8) {
         seconds = 0;
         minute++;
         if (minute > 4) {
            minute = 0;
            hour++;
            if (hour > 2) {
               hour = 0;
            }
         }
      }*/


/*
showInterval = () => {
   if (seconds < 10) {
      sec.innerText = "0" + seconds;
   } else { sec.innerText = seconds }
   if (seconds >= 59) {
      seconds = -1
   }
}*/