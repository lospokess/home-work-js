const arrowLinks = document.querySelectorAll('.arrow[data-goto]');

if (arrowLinks.length > 0) {
   arrowLinks.forEach(arrowLink => {
      arrowLink.addEventListener("click", onArrowClick);
      /*  arrowLink.addEventListener("click", addScroll); */
   });

   /*  function addScroll() {
       document.body.classList.add("scroll");
    } */

   function onArrowClick(e) {
      const arrowLink = e.target;
      /* arrowLink.style.transform = 'rotateX(360deg)'; */
      if (arrowLink.dataset.goto && document.querySelector(arrowLink.dataset.goto)) {
         const gotoBlock = document.querySelector(arrowLink.dataset.goto);
         const gotoBlockValue = gotoBlock.getBoundingClientRect().top + scrollY/*  - document.querySelector(".header").offsetHeight */;


         window.scrollTo({
            top: gotoBlockValue,
            behavior: "smooth",


         });

         /* e.preventDefault(); */
      }
   }
}