"use strict"
/*//!Создай класс, который будет создавать пользователей с именем и фамилией. Добавить к классу метод для вывода имени ифамилии */

/*//! Создай список состоящий из 4 листов. Используя джс обратись к 2 li и с использованием навигации по DOM дай 1 элементусиний фон, а 3 красный */

/*//! Создай див высотой 400 пикселей и добавь на него событие наведения мышки. При наведении мышки выведите текстомкоординаты, где находится курсор мышки
 */
/* //!Создай кнопки 1,2,3,4 и при нажатии на кнопку выведи информацию о том какая кнопка была нажата
 */
/* //!Создай див и сделай так, чтобы при наведении на див, див изменял свое положение на странице
 */
/* //!Создай поле для ввода цвета, когда пользователь выберет какой-то цвет сделай его фоном body */

/* //!Создай инпут для ввода логина, когда пользователь начнет Вводить данные в инпут выводи их в консоль */

/* //?Создайте поле для ввода данных поле введения данных выведите текст под полем  */

//!TASK_1==========
class User {
   constructor(firstName, lastName, age) {
      this.firstName = firstName;
      this.lastName = lastName;
      this.age = age;
   }
   showUser() {
      let show = document.querySelector(".task__1");
      show.prepend(`    ${this.firstName}  ${this.lastName}  ${this.age}`)
      return
   }
}

let user1 = new User("Микола", "Пацюк", "29 років."),
   user2 = new User("Данило", "Глід", "8 років");
//!TASK_2=======================================
//!TASK_3====================================

const mouse = {
   x: 0,
   y: 0
}
//!TASK_4,5,6,7,8==============================

document.addEventListener("DOMContentLoaded", (event) => {
   //!task1
   user2.showUser();
   user1.showUser();
   //!task2
   let ulList = document.querySelector("ul");
   ulList.lastElementChild.style.backgroundColor = "purple"
   ulList.firstElementChild.style.backgroundColor = "green"
   ulList.children[2].style.backgroundColor = "red"
   //!task3
   const task3 = document.querySelector(".task__3");
   task3.addEventListener("mouseover", (event) => {
      const rect = task3.getBoundingClientRect();
      mouse.x = Math.floor(event.clientX - rect.top) ;
      mouse.y = Math.floor(event.clientY - rect.top) ;
      task3.textContent =`x:${mouse.x/* .toFixed(2) */}  y:${mouse.y/* .toFixed(2) */}`
   })
   //!task4
   const btn = document.querySelectorAll("button")
   btn.forEach((event)=>{
      event.addEventListener("click", ()=> {
         alert(`НАТИСНУТА КНОПКА ${event.textContent}`)
      }) 
   })
   //!task5
   const div = document.querySelector(".task__5");
   let done = () => {
      div.style.marginLeft = "0px"
   }
   div.addEventListener("mouseover", () => {
      div.style.marginLeft = "300px"
      setTimeout(done,2000)
   })
   //!task6
   const inpColor = document.querySelector("input[type='color']")
      inpColor.addEventListener("input", ()=> {
      document.body.style.backgroundColor = inpColor.value;
   })
   //!task7
   const inpPassword = document.querySelector("input[type='password']")
   inpPassword.addEventListener("input"/* "change" */, () => {
      console.log(inpPassword.value)
   })
   //!task8
   const texterra = document.querySelector("textarea");
   texterra.addEventListener("change", () => {
      const boxInfo = document.querySelector(".task__8 div");
      boxInfo.textContent = texterra.value
      
   })
   
   
})
