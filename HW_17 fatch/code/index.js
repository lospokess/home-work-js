import { req, show, showNbu, showHeroes } from "./function.js"
/*
Зробити програму з навігаційним меню яке буде показувати один з варіантів. 
Курс валют НБУ з датою на який день,  https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json
героїв зоряних війн, https://swapi.dev/api/people/
список справ з https://jsonplaceholder.typicode.com/ виводити які виконані які та які ні з можливістю редагування
*/
let moreHeroes = document.querySelector("#heroes"),
n = 1;
if (localStorage.history === undefined) {
    localStorage.history = JSON.stringify([])
}

document.getElementById("todo").addEventListener("click", () => {
    moreHeroes.textContent = "ГЕРОЇ ЗОРЯНИХ ВІЙН"
    n = 1;
    req("https://jsonplaceholder.typicode.com/todos")
        .then(info => show(info));

})
document.getElementById("nbu").addEventListener("click", () => {
    moreHeroes.textContent = "ГЕРОЇ ЗОРЯНИХ ВІЙН"
    n = 1;
    req("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json")
        .then(info => showNbu(info))
})
moreHeroes.addEventListener("click", () => {
    if (moreHeroes.textContent === "ДОДАТИ ЩЕ ГЕРОЇВ") {
        n++
        if(n >9 ){
            moreHeroes.textContent = "ГЕРОЇ ЗОРЯНИХ ВІЙН"
            n = 1;
        }
        `${req(`https://swapi.dev/api/people/?page=${n}`)
                .then(info => showHeroes(info.results))}`
        console.log(n)
    } else {
        `${req(`https://swapi.dev/api/people/?page=${n}`)
                .then(info => showHeroes(info.results))}`
    }
    /* "https://swapi.dev/api/people/" */
    /* "https://swapi.dev/api/people/?page=3" */
    /* "https://swapi.dev/api/people/?page=4" */
    /* "https://swapi.dev/api/people/?page=5" */
})
document.querySelector("#close")
    .addEventListener("click", e => document.querySelector(".parent").classList.remove("active"))







