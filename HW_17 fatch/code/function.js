const spans = [],
display = document.querySelector(".display");

async function req(url) {
    document.querySelector(".loader").classList.add("active")
    const data = await fetch(url);
    return data.json()
}

function show(data) {
    const tableTodo = `
        <table>
         <thead>
          <tr>
           <th>
           №
           </th>
           <th>
           Задача
           </th>
           <th>
           Статус
           </th>
           <th>
           Редагувати
           </th>
          </tr>
         </thead>
         <tbody>
           ${data.map((obj, i) => {
       
        const span = document.createElement("span");
        span.addEventListener("click", () => {
            clickHendler(obj)
        })
        span.innerHTML = "&#128295;"
        spans.push(span)
        return `
               <tr>
                    <td>
                    ${obj.id}
                    </td>
                    <td>
                    ${obj.title
            }
                    </td>
                    <td>
                    ${obj.completed ? "&#9989;" : "&#10060;"}
                    </td>
                    <td data-span="${i}">
                      
                    </td>
                </tr>`
    }).join("")}
         </tbody>
         </tablr>
        `
    display.innerHTML = "";
    display.insertAdjacentHTML("beforeend", tableTodo)

    document.querySelectorAll("td[data-span]")
        .forEach((e, i) => {
            console.log()
            e.append(spans[i])
        })
    document.querySelector(".loader").classList.remove("active")
    console.log(data)
}

function clickHendler(obj) {
    document.querySelector(".parent").classList.add("active");
    document.getElementById("description").innerText = obj.title;
    document.getElementById("status").checked = obj.completed;

    document.getElementById("save").onclick = () => {
        console.log("+")
        const l = JSON.parse(localStorage.history);
        l.push(obj);
        localStorage.history = JSON.stringify(l)

        document.querySelector("#save")
            .addEventListener("click", e => document.querySelector(".parent").classList.remove("active"))
    }
}

function showNbu(data) {
    const tableNbu = `
    <table>
    <thead>
     <tr>
      <th>
      №
      </th>
      <th>
      Назва
      </th>
      <th>
      Ціна
      </th>
     </tr>
    </thead>
    <tbody>
    ${data.map((obj, i) => {
        return `
        <tr>
            <td>
            ${i}
            </td>
            <td>
            ${obj.txt}
            </td>
            <td>
            ${obj.rate}
            </td>
    </tr>`
    }).join("")}
       
    </tbody>
    </table>
    `
    console.log(data)
    display.innerHTML = "";
    display.insertAdjacentHTML("beforeend", tableNbu)
    document.querySelector(".loader").classList.remove("active")
}

//?============================Heroes
function showHeroes(data) {
    const tableHeroes = `
    <table>
    <thead>
     <tr>
     <th>
      №
      </th>
      <th>
      Name
      </th>
      <th>
      Height
      </th>
      <th>
      Mass
      </th>
      <th>
      Hair color
      </th>
      <th>
      Skin color
      </th>
     </tr>
    </thead>
    <tbody>
    ${data.map((obj, i) => {
        return `
        <tr>
            <td>
            ${i + 1}
            </td>
            <td>
            ${obj.name}
            </td>
            <td>
            ${obj.height}
            </td>
            <td>
            ${obj.mass}
            </td>
            <td>
            ${obj.hair_color}
            </td>
            <td>
            ${obj.skin_color}
            </td>
    </tr>`
    }).join("")}
    </tbody>
    
    </table>
    `
    console.log(data);
    display.innerHTML = "";
    display.insertAdjacentHTML("beforeend", tableHeroes);
    let moreHeroes = document.querySelector("#heroes");
    console.dir(moreHeroes)
    moreHeroes.innerText = "ДОДАТИ ЩЕ ГЕРОЇВ"
    document.querySelector(".loader").classList.remove("active");
}
export { req, show, showNbu, showHeroes };