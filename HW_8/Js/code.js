/*ДЗ :
//!Створити клас Car , Engine та Driver.

//!(1)Клас Driver містить поля - ПІБ, стаж водіння.

//!(2))Клас Engine містить поля – потужність, виробник.

//!(3)Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine. 

//!(4)Методи start(), stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч". А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна.


//!(5)Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.

//!(6)Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.
*/

class Driver {
   constructor(fullName, experience) {
      //super(fullName);
      this.fullName = fullName;
      this.experience = experience;
   }
}

class Engine extends Driver {
   constructor(power, producer, fullName, experience) {
      //super(power);
      super(fullName, experience);
      this.power = power;
      this.producer = producer;
   }
}

class Car extends Engine {
   constructor(brandAuto, classAuto, weightAuto, power, producer, fullName, experience/*Driver, Engine*/) {
      super(power, producer, fullName, experience)
      this.brandAuto = brandAuto;
      this.classAuto = classAuto;
      this.weightAuto = weightAuto;
      /*this.Driver = {
         constructor(fullName, experience) {
            this.super = fullName;
            this.experience = experience;
         }
      }
      this.Engine = {
         constructor(power, producer) {
            this.power = power;
            this.producer = producer;
         }
      }*/
   }
   start() {
      document.write(`<input type = "button" value = "Поїхали">`)
   }
   stop() {
      document.write(`<input type = "button" value ="Зупиняємося" >`)
   }
   turnRight() {
      document.write(`<input type = "button" value ="Поворот праворуч" >`)
   }
   turnLeft() {
      document.write(`<input type = "button" value = "Поворот ліворуч">`)
   }
   toString() {
      for (const key in this) {
         document.write(`<ul> <li>${key} : ${this[key]}</li></ul>  `)
      }
   }
};

//let car1 = new Car('mersedes', 'S-class', '2600', ('djo-djo-alen', '28 yers'), ('270 k.s.', 'mers'));
//console.log(car1);

class Lorry extends Car {
   constructor(brandAuto, classAuto, weightAuto, power, producer, fullName, experience, carryingCapacity) {
      super(brandAuto, classAuto, weightAuto, power, producer, fullName, experience)
      this.carryingCapacity = carryingCapacity;
   }
}

class SportCar extends Car {
   constructor(brandAuto, classAuto, weightAuto, power, producer, fullName, experience, speed) {
      super(brandAuto, classAuto, weightAuto, power, producer, fullName, experience,)
      this.speed = speed;
   }
};

const driver = new Driver();
const engine = new Engine("178 к.с.", "Hyundai Motor Company");
const hyundai = new Car("Hyundai",
"Бізнес клас",
"1538–1760 кг",
"178 к.с.",
"Hyundai Motor Company",
"Djo Djo Alen",
"20 р." );
const lorry = new Lorry("Volvo FH",
"Trucks",
"6620–11095 кг",
"750 к.с.",
"Volvo Trucks",
"Met Gil Nilsen",
"35 р.",
" 295 тонн" );
const sportCar = new SportCar("Ferrari SF1000",
"Формула-1",
"746 кг",
"515 к.с.",
"Scuderia Ferrari",
"Sebastian Vettel",
"20+ р.",
"322,7 км/год" );

console.log(driver);
console.log(engine);
console.log(hyundai);
console.log(lorry);
console.log(sportCar);

hyundai.toString();
hyundai.start();
hyundai.stop();
hyundai.turnLeft();
hyundai.turnRight();

lorry.toString();
lorry.start();
lorry.stop();
lorry.turnLeft();
lorry.turnRight();

sportCar.toString();
sportCar.start();
sportCar.stop();
sportCar.turnLeft();
sportCar.turnRight()


