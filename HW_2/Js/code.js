/*
!Задача 1
?Використовуючи пробіли &nbsp; та зірочки * намалюйте використовуючи цикли
?Трикутник
?Ромб
?Порожній прямокутник
*/


for (let i = 0; i < 20; i++) {
   for (let j = 0; j < 20; j++) {
      if (i === 0 || i === 19) {
         document.write("*")
      } else {
         document.write("&nbsp;&nbsp;")
      }
      if (j === 0 || j === 19) {
         document.write("*")
      } else {
         document.write("&nbsp;")
      }
   }
   document.write("<br>")
}
document.write("<br>");
document.write("<br>");
document.write("<br>");

for (let i = 0; i < 20; i++) {
   for (let j = 0 + i; j < 20; j++) {
      document.write("*")
   }
   document.write("<br>")
}
document.write("<br>");
document.write("<br>");
document.write("<br>");

for (let i = 0; i < 10; i++) {
   for (let j = 0 + i; j < 10; j++) {
      document.write("*")
   }
   document.write("<br>")
}
for (let i = 0; i < 11; i++) {
   for (let j = 0 - i; j < 0; j++) {
      document.write("*")
   }
   document.write("<br>")
}
for (let i = 0; i < 10; i++) {
   for (let j = 0 + i; j < 10; j++) {
      document.write("*")
   }
   document.write("<br>")
}5
/*
!Задача 2
?Реалізувати програму на Javascript, яка буде знаходити всі числа кратні 5 (діляться на 5 без залишку) у заданому діапазоні.

?- Висести за допомогою модального вікна браузера число, яке введе користувач.
?- Вивести в консолі усі числа кратні 5, від 0 до введеного користувачем числа. Якщо таких чисел немає - вивести в консоль фразу Sorry, no numbers
?- Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.

?- Перевірити, чи введене значення є цілим числом. Якщо ця умова не дотримується, повторювати виведення вікна на екран, доки не буде введено ціле число.

*/
let a = parseFloat(prompt("Ведіть ціле число!", "5 - 10 000"));

while (Number.isInteger (a) === false){//! Чи можливо зробити це без допомоги (Number.isInteger) якщо (так) напишіть у коментарях 
   a = parseFloat(prompt("Ведіть ціле число!", "5 - 10 000"));
}

if(a >=5 && a <= 10000){
   for (let i = 0; i <= a; i++) {
      if (i % 5 === 0){
         console.log(i);
      }
   }
}else{
   console.error("Sorry, no numbers");
}

alert(`Ви увели номер: ${a}`);

/*//!vi co test
function validateNumber (number) {

    if(isNaN(number) === false && typeof number === "number") {
        console.log("number");
    }

    while (isNaN(number)) {
        console.log("+"); 
        number = parseFloat(prompt("Введіть дані: "));
        if(isNaN(number) === false && typeof number === "number") {
            console.log("number");
        }
    }
    console.log("-");
    return number;
   
}

*/