"use strict"
const field = document.querySelector(".field"),//!поле
   scoreOne = document.querySelector(".score__one"),//!рахунок 1
   scoreTwo = document.querySelector(".score__two"),//!рахунок 2
   palyerName = document.querySelector(".player__name"),//!поле воду імен
   btnStartGame = document.querySelector(".btn__start-game"),//!кнопка старт
   btnTableWin = document.querySelector(".teble__win"),//! кнопка таблиця переможців
   btnRemoveTable = document.querySelector(".remove__box"),//!кнопка закрити таблицю
   btnInfoDeveloper = document.querySelector(".info__developer"),//! кнопка інформація про розроб...
   boxEmblem = document.querySelector(".box__emblem"),//! завантаження гри
   tableWin = document.querySelector(".box__win"),//! бокс таблиція перем...
   playerWin = document.querySelector(".player__win"),//!вивід переможця
   boxInfo = document.querySelector(".box__info"),//! бокс інфо про розробника
   btnRemoveBoxInfo = document.querySelector(".remove__box-info"),//!кнопка закрити інфо про розроб...
   boxVs = document.querySelector(".box__vs"),//!бокс ігор інформація 
   inputPlayer1 = document.querySelector(".player__1 > input"),
   inputPlayer2 = document.querySelector(".player__2 > input");
//?flag
let btnCheck = null,
   startInterval = true,
   start = true,
   numberGame = 0,//?номер гри
   redScore = 0,//?рахунок 
   blueScore = 0,//?рахунок
   win1 = "",//?нагорода
   win2 = "";//?нагорода

let palyerName1 = document.querySelector(".palyer__name1"),
   palyerName2 = document.querySelector(".palyer__name2");

let leftPlatform = document.querySelector(".platform__left"),
   rightPlatform = document.querySelector(".platform__right"),
   ball = document.querySelector(".ball");

let fieldWidth = 1000,
   fieldHeight = 600;

let ballWidth = 30,
   ballHeight = 30;

let platformHeight = 200,
   platformWidth = 25,
   redPlayerY = (fieldHeight - platformHeight) / 2,
   redPlayerX = fieldWidth - 955 - platformWidth,
   bluePlayerY = (fieldHeight - platformHeight) / 2,
   bluePlayerX = 20;
//?кординати мя'ча
let ballX;
let ballY;
//?попередні кординати
let ballLastX;
let ballLastY;
//?швидкість з якою рухється мя'ч
let ballSpeedX;
let ballSpeedY;

/* let ballLastSpeedX = ballSpeedX;
let ballLastSpeedY = ballSpeedY; */
//?швидкість з якою рухаються плптформи
let redPlayerSpeedY = 0;
let bluePlayerSpeedY = 0;

//?   ЗАВАНТАЖЕННЯ СТОРІНКИ~
document.addEventListener("DOMContentLoaded", () => {
   setTimeout(removeBoxElement, 1000); //!зфбираєм заставку
   btnStartGame.addEventListener("click", hendlerBtnNamePlayer);

   btnTableWin.addEventListener("click", hendlerTablelWin);
   btnRemoveTable.addEventListener("click", hendlerRemoveTableWin);

   btnInfoDeveloper.addEventListener("click", hendlerInfo);
   btnRemoveBoxInfo.addEventListener("click", hendlerRemoveBoxInfo);

   document.addEventListener("keydown", hendlerKeyDown);
   document.addEventListener("keyup", hendlerKeyUp);

   const tick = () => {
      moveObjects();
      checkCollisions();
      updateHtml();
      checkGoal();
   }
   setInterval(tick, 1000 / 25);
})
//?(TICK START)
let moveObjects = () => {
   ballLastX = ballX;
   ballLastY = ballY;

   ballX = ballX + ballSpeedX;
   ballY = ballY + ballSpeedY;

   redPlayerY = redPlayerY + redPlayerSpeedY;
   bluePlayerY = bluePlayerY + bluePlayerSpeedY;
}
let checkCollisions = () => {
   if (ballY > fieldHeight - ballHeight) {
      ballSpeedY = -ballSpeedY;
      /* ballLastSpeedY = -ballSpeedY; */
   }
   if (ballY < 0) {
      ballSpeedY = -ballSpeedY;
   }
   if (redPlayerY > fieldHeight - platformHeight) {
      redPlayerY = fieldHeight - platformHeight;
      redPlayerSpeedY = 0;
   }
   if (bluePlayerY > fieldHeight - platformHeight) {
      bluePlayerY = fieldHeight - platformHeight;
      bluePlayerSpeedY = 0;
   }
   if (redPlayerY < 0) {
      redPlayerY = 0;
      redPlayerSpeedY = 0;
   }
   if (bluePlayerY < 0) {
      bluePlayerY = 0;
      bluePlayerSpeedY = 0;
   }
   //!Удар в платформу
   if (ballY + ballHeight / 2 > bluePlayerY && ballY + ballHeight / 2 < bluePlayerY + platformHeight) {
      if (ballX + ballWidth > bluePlayerX + 935) {
         if (ballLastX + ballWidth <= bluePlayerX + 935) {
            ballSpeedX++;
            ballSpeedX = -ballSpeedX;
            /* ballLastSpeedX = ballSpeedX; */
            ballX = (bluePlayerX + 935) - ballWidth;
         }
      }
   }
   if (ballY + ballHeight / 2 > redPlayerY && ballY + ballHeight / 2 < redPlayerY + platformHeight) {
      if (ballX + ballWidth > redPlayerX) {
         if (ballLastX + ballWidth <= redPlayerX + platformWidth + 25) {
            ballSpeedX--;
            ballSpeedX = -ballSpeedX;
            /* ballLastSpeedX = ballSpeedX; */
            ballX = (redPlayerX + platformWidth + 30) - ballWidth;
         }
      }
   }
}
let updateHtml = () => {
   ball.style.left = ballX + "px";
   ball.style.top = ballY + "px";

   leftPlatform.style.left = redPlayerX + "px";
   leftPlatform.style.top = redPlayerY + "px";
   rightPlatform.style.right = bluePlayerX + "px";
   rightPlatform.style.top = bluePlayerY + "px";
}
let checkGoal = () => {
   if (ballX < 0) {
      scoreTwo.innerHTML = blueScore += 1;
      restartBallWinBlue();
      if (blueScore === 6) {
         numberGame += 1;
         win2 = "&#127941;";
         create();
         stopBall();
         playerWin.style.zIndex = "2";
         playerWin.style.color = "blue";
         playerWin.innerHTML = `${palyerName2.innerHTML} Win`;
         btnStartGame.style.animation = "padd 1.1s ease infinite";
         scoreTwo.innerHTML = 0;
         blueScore = 0;
         scoreOne.innerHTML = 0;
         redScore = 0;
         startInterval = false;
         if (startInterval === false) {
            btnStartGame.addEventListener("click", hendlerResetBall);
         } else if (startInterval === true) {
            btnStartGame.addEventListener("click", removeHendlerResetBall);
         }
      } else {
         win2 = "";
      }
   }
   if (ballX > fieldWidth - ballHeight) {
      scoreOne.innerHTML = redScore += 1;
      restartBallWinRed();
      if (redScore === 6) {
         numberGame += 1;
         win1 = "&#127941;";
         create();
         stopBall();
         playerWin.style.zIndex = "2";
         playerWin.style.color = "red";
         playerWin.innerHTML = `${palyerName1.innerHTML} Win`;
         btnStartGame.style.animation = "padd 1.1s ease infinite";
         scoreOne.innerHTML = 0;
         redScore = 0;
         scoreTwo.innerHTML = 0;
         blueScore = 0;
         startInterval = false;
         if (startInterval === false) {
            btnStartGame.addEventListener("click", hendlerResetBall);
         } else if (startInterval === true) {
            btnStartGame.addEventListener("click", removeHendlerResetBall);
         }
      } else {
         win1 = "";
      }
   }
}
let hendlerKeyDown = (event) => {
   //w=87 s=83 стрілка верх=38 стрілка в низ=40
   if (btnCheck === true) {
      switch (event.code) {
         case "KeyW": redPlayerSpeedY = -35;
            break;
         case "KeyS": redPlayerSpeedY = 35;
            break;
         case "ArrowUp": bluePlayerSpeedY = -35;
            break;
         case "ArrowDown": bluePlayerSpeedY = 35;
            break;
         default:
         /* console.log(event.code);
      console.log(event)
      console.dir(field) */
      }
   }
}
let hendlerKeyUp = (event) => {
   if(btnCheck === true) {
      switch (event.code) {
      case "ArrowUp":
      case "ArrowDown":
         bluePlayerSpeedY = 0;
         break;
      case "KeyW":
      case "KeyS":
         redPlayerSpeedY = 0;
         break;
      default:
      /* console.log(event.code); */
   }
   }
}
//?(RESTART)
let hendlerResetBall = () => {
   if (startInterval === false) {
      setTimeout(restartBall, 1500);
      startInterval = true;
   }
   hendlerRemoveTableWin();
   hendlerRemoveBoxInfo();
   playerWin.style.zIndex = "0";
   btnStartGame.style.animation = "";
}
let removeHendlerResetBall = () => {
   btnStartGame.removeEventListener("click", hendlerResetBall);
}
let restartBall = () => {
   ballX = (fieldWidth - ballWidth) / 2;
   ballY = (fieldHeight - ballHeight) / 2;

   ballLastX = ballX;
   ballLastY = ballY;

   ballSpeedX = 18; /* (Math.random() - 0.5) * 60; */
   ballSpeedY = 14;/* (20 + Math.random() * 1) * Math.sign(Math.random() - 0.5); */

   /*  ballLastSpeedX = ballSpeedX;
      ballLastSpeedY = ballSpeedY; */
}
let restartBallWinBlue = () => {
   ballX = (fieldWidth - ballWidth) / 2;
   ballY = (fieldHeight - ballHeight) / 2;

   ballLastX = ballX;
   ballLastY = ballY;

   ballSpeedX = (Math.random() + 10 * 2);  /* 18; */
   ballSpeedY = (10 + Math.random() * 5) * Math.sign(Math.random() - 0.5);  /* 16; */
}
let restartBallWinRed = () => {
   ballX = (fieldWidth - ballWidth) / 2;
   ballY = (fieldHeight - ballHeight) / 2;

   ballLastX = ballX;
   ballLastY = ballY;

   ballSpeedX = (Math.random() - 10 * 2);  /* -18 */
   ballSpeedY = (10 + Math.random() * 5) * Math.sign(Math.random() - 0.5);  /* -16; */
}
let stopBall = () => {
   ballX = (fieldWidth - ballWidth) / 2;
   ballY = (fieldHeight - ballHeight) / 2;

   ballLastX = ballX;
   ballLastY = ballY;

   ballSpeedX = 0;
   ballSpeedY = 0;

}
//?(TICK END)
//?(1)
let removeBoxElement = () => {
   boxEmblem.style.display = "none";
   btnStartGame.style.animation = "padd 1.1s ease infinite";
   btnCheck = false;
}
//?(2)
let hendlerBtnNamePlayer = () => {
   if (btnCheck === false) {
      palyerName1.innerHTML = inputPlayer1.value;
      palyerName2.innerHTML = inputPlayer2.value;
      if (inputPlayer1.value === "") {
         palyerName1.innerHTML = "Player 1";
      }
      if (inputPlayer2.value === "") {
         palyerName2.innerHTML = "Player 2";
      }
      palyerName.style.display = "none";
      btnStartGame.style.animation = "";
      btnStartGame.innerHTML = "RESET";
      stopBall();
      if (!btnCheck) {
         btnCheck = true;
         setTimeout(restartBall, 1500);
      }
   }
}
//?(3)
let hendlerTablelWin = () => {
   tableWin.style.zIndex = "3";
   boxInfo.style.zIndex = "0";
}
let hendlerRemoveTableWin = () => {
   tableWin.style.zIndex = "0";
}
let create = () => {
   boxVs.insertAdjacentHTML("beforeend", `<div class="vs__game">
   <div class="namber__game">${numberGame}</div>
   <div class="box__player">
      <div class="player1__vs"><span class="win1">${win1}</span>${palyerName1.textContent}</div>
      <span class="vs">VS</span>
      <div class="player2__vs">${palyerName2.textContent}<span class="win2">${win2}</span></div>
   </div>
   <div class="score__game">${redScore}/${blueScore}</div>
   </div>
   </div>`);
}
//?(4)
let hendlerInfo = () => {
   boxInfo.style.zIndex = "3";
   tableWin.style.zIndex = "0";
}
let hendlerRemoveBoxInfo = () => {
   boxInfo.style.zIndex = "0";
}
/*
//!від 
let hendlerStop = () => {
   if (start === true) {
      ballLastX = ballX;
      ballLastY = ballY;

      ballSpeedX = 0;
      ballSpeedY = 0;
      console.log(ballSpeedX, ballSpeedY, start);
      start = false;
   }

}
let hendlerMove = () => {
   if (start === false) {
      ballLastX = ballX;
      ballLastY = ballY;

      ballSpeedX = ballLastSpeedX + ballSpeedX;
      ballSpeedY = ballLastSpeedY + ballSpeedY;

      if (ballLastX < (fieldWidth / 2)) {
         ballSpeedX = (ballLastSpeedX + ballSpeedX);
         ballSpeedY = ballLastSpeedY + ballSpeedY;
      }
      start = true;
   }
   console.log(ballSpeedX, ballSpeedY, start);
}
 //!до
 */