/*
!У папці calculator дана верстка макета калькулятора. Необхідно зробити цей калькулятор робітником.

!!При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.

! При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма чекає введення другого числа для виконання операції.

* Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, так і будь-якого з операторів, в табло повинен з'явитися результат виконання попереднього виразу.

! При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число. Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.
*/
let calc = {
   value1: "",
   value2: "",
   sign: "",
   memoryV: ""
},
   finish = false,
   equals = document.querySelector("#rez"),
   mrc = document.querySelector("#mrc"),
   mrcRes = 0;

const digit = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "."],
   action = ["-", "+", "*", "/"],
   memory = ["mrc", "m-", "m+"];

//display.value = "00"
//console.log(display.textContent);


window.addEventListener("DOMContentLoaded", () => {
   const btn = document.querySelector(".keys"),
      display = document.querySelector(".display > input"),
      c = document.querySelector(".c").addEventListener("click", () => {
         calc.value1 = "";//first number
         calc.value2 = "";//second number 
         calc.sign = "";//знак операції
         finish = false;
         display.value = 0;
         console.log(c);
      })

   btn.addEventListener("click", (e) => {
      if (!e.target.classList.contains("button")) return;
      //if (e.target.classList.contains("c")) return;
      //calc.value1 += e.target.value
      //show(calc.value1, display)
      display.value = "";

      const key = e.target.value

      //якщо натиснута кнопка mrc m+ m-
      if (memory.includes(key)) {
         if (key === "m+") {
            calc.memoryV = calc.value1
            mrc.textContent = "m"
            console.log("є контакт  m+")
         } else if (key === "m-") {
            calc.memoryV = calc.value1;
            mrc.textContent = "m";
            console.log("є контакт  m-")
         } else if (key === "mrc") {
            mrc.textContent = calc.memoryV
            mrcRes++;
            console.log("1 зовнішня", mrcRes)
            if (mrcRes >= 2) {
               mrc.textContent = "";
               calc.memoryV = "";
               mrcRes = 0;
               console.log("2 внутрішня", mrcRes)
            }
         }
         /*
         if (mrcRes > 1) {
            mrc.textContent = "";
            calc.memoryV = "";
            console.log("2", mrcRes)
         }
         */
         return
      }
      //якщо натиснута клавіша 0-9 або . 
      if (digit.includes(key)) {
         if (calc.value2 === "" && calc.sign === "") {
            calc.value1 += key;
            console.log(calc.value1, calc.value2, calc.sign);
            display.value = calc.value1;

         } else if (calc.value1 !== "" && calc.value2 !== "" && finish) {
            calc.value2 = key;
            finish = false;
            display.value = calc.value2;

         } else {
            calc.value2 += key;
            display.value = calc.value2;
            equals.disabled = false;
         }
         console.log(calc.value1, calc.sign, calc.value2);
         return
      }
      //якщо натиснута клавіша + - . * 
      if (action.includes(key)) {
         calc.sign = key;
         console.log(calc.sign);
         // display.value = calc.sign;
         return
      }
      //натиснутий знак =
      if (key === "=") {
         if (calc.value2 === "") calc.value2 = calc.value1;
         switch (calc.sign) {
            case "+": calc.value1 = (+calc.value1) + (+calc.value2);
               break;
            case "-": calc.value1 = calc.value1 - calc.value2;
               break;
            case "*": calc.value1 = calc.value1 * calc.value2;
               break;
            case "/":
               if (calc.value2 === "0") {
                  display.value = "Error";
                  calc.value1 = "";
                  calc.value2 = "";
                  calc.sign = "";
                  return
               }
               calc.value1 = calc.value1 / calc.value2;
               break;
         }
         finish = true;
         display.value = calc.value1;
         console.log(calc.value1, calc.sign, calc.value2);
      }
   });
});

show = (value, el,) => {
   el.value = value
}


/*
 if (e.target.value === sign) {
         show(calc1.value2, display)
         if(e.target.value === sign){
            mul()
         }
         
         console.log("*")
      }
      else if (e.target.value === sign) {
         show(calc1.value2, display)
         console.log("-")
      }
      else if (e.target.value === sign) {
         console.log("+")
         show(calc1.value2, display)
      }
      else if (e.target.value === sign) {
         console.log("/")
      }
      else if (e.target.value === sign) {
         console.log("=")
      }
      switch (sign) {
         case "+": show(calc(value1, value2, add));
            break;
         case "-": show(calc(value1, value2, sub));
            break;
         case "*": show(calc(value1, value2, mul));
            break;
         case "/": show(calc(value1, value2, div));
            break;
      }
const add = (value1, value2) => {
   return value1 + value2;
}
const div = (value1, value2) => {
   if (value2 === 0) {
      console.error("На 0 не ділять!!!");
      return
   }
   return value1 / value2;
}
const mul = (value1, value2) => {
   return value1 * value2
}
const sub = (value1, value2) => {
   return value1 - value2;
}
const show2 = (rez) => {
   console.log(rez)
}
function calc(value1 = 0, value2 = 0, callback) {
   return callback(value1, value2);
}
*/


/*
switch (sign) {
   case "+": return add(value1, value2);
   case "-": return sub(value1, value2);
   case "*": return mul(value1, value2);
   case "/": return div(value1, value2);
}
*/