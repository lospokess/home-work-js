/*
При загрузке страницы - показать на ней кнопку с текстом "Нарисовать круг". Данная кнопка должна являться единственным контентом в теле HTML документа, весь остальной контент должен быть создан и добавлен на страницу с помощью Javascript
При нажатии на кнопку "Нарисовать круг" показывать одно поле ввода - диаметр круга. При нажатии на кнопку "Нарисовать" создать на странице 100 кругов (10*10) случайного цвета. При клике на конкретный круг - этот круг должен исчезать, при этом пустое место заполняться, то есть все остальные круги сдвигаются влево.
*/

//!Хотів б побачити правильне виконання задачі (абож у вашому виконанні)


let input1 = document.getElementById("input1")
let btn = document.querySelector('button');
//input1.placeholder = "Ведіть діаметер";
input1.setAttribute("placeholder", "Ведіть діаметер");


function constructCircle(diameter) {
   for(let i = 0; i < 100; i++){
      document.body.prepend(document.createElement("div"));
   }
}


btn.onclick = function () {
   let diameter = input1.value;
   constructCircle(diameter);
   let [...div] = document.getElementsByTagName('div');
   div.forEach(function (item) {
      item.onclick = function () {
         item.remove()
      }
   });

   div.forEach(function (item) {
      item.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)},100%,50%)`
      item.style.width = `${diameter * 10 + 10}px`
      item.style.height = `${diameter * 10 + 10}px`
      return
   })
   input1.remove();
   btn.remove();
}





/*
let blok = document.createElement("div");
      blok.style.width = `${diameter*10+60}px`
      blok.style.height = `${diameter*10+60}px`
*/
/*
let [...div] = document.getElementsByTagName('div');
div.forEach(function (item) {
   item.onclick = function () {
      item.remove()
   }
});
*/

/*
   input.forEach(function (item) {
      item.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)},100%,50%)`
      return
   })
*/